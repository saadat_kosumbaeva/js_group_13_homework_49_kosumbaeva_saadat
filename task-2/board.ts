const createCheckerboard = function (size: number) {
    for (let i = 0; i < size; i++) {
        let line;
        for (let j = 0; j < size; j++) {
            if (i % 2 === j % 2) {
                if (!line) {
                    line = "██";
                } else {
                    line += "██";
                }
            } else {
                if (!line) {
                    line = "  ";
                } else {
                    line += "  ";
                }
            }
            if (j === size - 1) {
                console.log(line);
            }
        }
    }
};

createCheckerboard(8);